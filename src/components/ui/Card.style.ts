import fonts from "./../../constants/fonts";
import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import colors from "../../constants/colors";

export default StyleSheet.create({
  card: {
    borderRadius: RFValue(15),
    padding: RFValue(15),
    backgroundColor: colors.white,
  },
});
