import React, { FunctionComponent } from "react";
import {
  SafeAreaView,
  StyleProp,
  ViewStyle,
  StyleSheet,
  View,
} from "react-native";

type Props = {
  style?: StyleProp<ViewStyle>;
};

const Screen: FunctionComponent<Props> = (props) => {
  const { children, style } = props;

  return (
    <SafeAreaView style={style}>
      <View style={styles.container}>{children}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
});

export default Screen;
