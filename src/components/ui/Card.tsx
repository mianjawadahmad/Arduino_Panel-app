import React, { FunctionComponent } from "react";
import { View, ViewProps } from "react-native";
import styles from "./Card.style";

const Card: FunctionComponent<ViewProps> = (props) => {
  const { children, style } = props;

  return <View style={[styles.card, style]}>{children}</View>;
};

export default Card;
