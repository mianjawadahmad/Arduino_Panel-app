import React from "react";
import { View } from "react-native";
import { Text } from "../ui";
import styles from "./Header.style";

const Header = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Arduino Panel</Text>
    </View>
  );
};

export default Header;
