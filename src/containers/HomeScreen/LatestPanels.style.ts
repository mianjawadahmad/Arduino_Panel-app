import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import fonts from "../../constants/fonts";

export default StyleSheet.create({
  container: {
    marginTop: RFValue(20),
  },
  title: {
    fontSize: RFValue(20),
    fontFamily: fonts.primarySemiBold,
    marginBottom: RFValue(15),
  },
});
