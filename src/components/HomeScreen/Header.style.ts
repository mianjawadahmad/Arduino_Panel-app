import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import colors from "../../constants/colors";
import fonts from "../../constants/fonts";

export default StyleSheet.create({
  container: {
    marginTop: RFValue(20),
    paddingHorizontal: RFValue(25),
  },
  title: {
    fontSize: RFValue(24),
    fontFamily: fonts.primarySemiBold,
    color: colors.white,
  },
});
