import AsyncStorage from "@react-native-community/async-storage";
import { combineReducers, createStore } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import Panels from "./panels/reducer";

const appReducer = combineReducers({
  Panels,
});

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, appReducer);

export const store = createStore(persistedReducer);
export const persistor = persistStore(store);
