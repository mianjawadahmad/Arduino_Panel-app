import AsyncStorage from "@react-native-community/async-storage";
import Axios from "axios";
import { Formik, FormikHelpers } from "formik";
import React, { useState, useEffect } from "react";
import {
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  View,
} from "react-native";
import * as Yup from "yup";
import { TextInput } from "../../components/ui";
import colors from "../../constants/colors";
import colorsPreset from "../../data/colorsPreset.json";
import stripTrailingSlash from "../../utils/stripTrailingSlash";
import { Button, Text } from "../ui";
import Card from "../ui/Card";
import styles from "./NewPanel.style";
import { useSelector, RootStateOrAny, useDispatch } from "react-redux";
import { setApiUrl, addPanel, setNewPanel } from "../../store/panels/actions";

const validation = Yup.object().shape({
  apiUrl: Yup.string().required(),
  line1: Yup.string().required(),
  line2: Yup.string(),
  color: Yup.string(),
});

interface FormValues {
  apiUrl: string;
  line1: string;
  line2: string;
  color: string;
}

const NewPanel = () => {
  const apiUrl = useSelector(({ Panels }: RootStateOrAny) => Panels.apiUrl);
  const newPanel = useSelector(({ Panels }: RootStateOrAny) => Panels.newPanel);

  const [form, setForm] = useState<FormValues>({
    apiUrl,
    line1: "",
    line2: "",
    color: "#FF0000",
  });

  const dispatch = useDispatch();

  useEffect(() => {
    if (newPanel) {
      setForm({ ...form, ...newPanel });
    }
  }, [newPanel]);

  const onSubmit = async (
    values: FormValues,
    { setSubmitting }: FormikHelpers<FormValues>
  ) => {
    setSubmitting(true);

    await Axios.post(`${stripTrailingSlash(values.apiUrl)}/tasks`, {
      line1: values.line1,
      line2: values.line2,
      color: values.color,
    });

    dispatch(setApiUrl(values.apiUrl));
    dispatch(addPanel(values, 3));

    setSubmitting(false);
  };

  return (
    <View>
      <Card>
        <Text style={styles.title}>New Panel</Text>

        <Formik
          onSubmit={onSubmit}
          validationSchema={validation}
          initialValues={form}
          enableReinitialize
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            values,
            errors,
            touched,
          }) => (
            <View>
              <TextInput
                placeholder="API URL"
                style={styles.input}
                value={values.apiUrl}
                onChangeText={handleChange("apiUrl")}
                onBlur={handleBlur("apiUrl")}
                isInvalid={!!(errors.apiUrl && touched.apiUrl)}
                autoCorrect={false}
                textContentType="URL"
                keyboardType="url"
                dataDetectorTypes="link"
                autoCapitalize="none"
              />
              <TextInput
                placeholder="Line 1"
                style={styles.input}
                value={values.line1}
                onChangeText={handleChange("line1")}
                onBlur={handleBlur("line1")}
                isInvalid={!!(errors.line1 && touched.line1)}
              />
              <TextInput
                placeholder="Line 2"
                style={styles.input}
                value={values.line2}
                onChangeText={handleChange("line2")}
                onBlur={handleBlur("line2")}
                isInvalid={!!(errors.line2 && touched.line2)}
              />
              <ScrollView
                contentContainerStyle={styles.colorContainer}
                horizontal
              >
                {colorsPreset.map((color) => (
                  <TouchableOpacity
                    key={color}
                    onPress={() =>
                      handleChange("color")(color === values.color ? "" : color)
                    }
                  >
                    <View
                      style={[
                        styles.colorPreview,
                        { ...(values.color === color && styles.colorSelected) },
                        { backgroundColor: color },
                      ]}
                    />
                  </TouchableOpacity>
                ))}
              </ScrollView>

              <Button
                style={styles.button}
                onPress={() => handleSubmit()}
                disabled={isSubmitting}
              >
                {isSubmitting ? (
                  <ActivityIndicator color={colors.white} />
                ) : (
                  "Confirm"
                )}
              </Button>
            </View>
          )}
        </Formik>
      </Card>
    </View>
  );
};

export default NewPanel;
