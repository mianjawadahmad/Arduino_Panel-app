import fonts from "./../../constants/fonts";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  text: {
    fontFamily: fonts.primary,
  },
});
