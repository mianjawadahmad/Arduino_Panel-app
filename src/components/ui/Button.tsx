import React, { FunctionComponent } from "react";
import {
  TouchableOpacity,
  TouchableOpacityProps,
  StyleProp,
  TextStyle,
  View,
} from "react-native";
import Text from "./Text";
import styles from "./Button.style";

interface Props extends TouchableOpacityProps {
  textStyle?: StyleProp<TextStyle>;
}

const Button: FunctionComponent<Props> = (props) => {
  const { children, style, textStyle } = props;

  return (
    <TouchableOpacity {...props} style={[styles.button, style]}>
      <Text style={[styles.text, textStyle]}>{children}</Text>
    </TouchableOpacity>
  );
};

export default Button;
