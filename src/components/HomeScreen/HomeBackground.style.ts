import { StyleSheet, Dimensions } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import colors from "../../constants/colors";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundContainer: {
    position: "absolute",
    left: 0,
    right: 0,
    height: "100%",
    backgroundColor: colors.screenBackground,
  },
  contentContainer: {
    paddingHorizontal: RFValue(25),
    paddingTop: Dimensions.get('window').height > 600 ? RFValue(60): RFValue(20),
    flexGrow: 1,
  },
  svg: {
    position: "absolute",
  },
});
