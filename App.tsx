import { AppLoading } from "expo";
import * as Font from "expo-font";
import React, { useState, useEffect } from "react";
import HomeScreen from "./src/screens/HomeScreen";
import { Provider } from "react-redux";
import { store, persistor } from "./src/store";
import { PersistGate } from "redux-persist/integration/react";

export default function App() {
  const [loaded, setLoaded] = useState(false);

  const loadFonts = async () => {
    await Font.loadAsync({
      "cabin-regular": require("./src/assets/fonts/Cabin-Regular.ttf"),
      "cabin-semibold": require("./src/assets/fonts/Cabin-SemiBold.ttf"),
      "cabin-bold": require("./src/assets/fonts/Cabin-Bold.ttf"),
    });

    setLoaded(true);
  };

  useEffect(() => {
    loadFonts();
  }, []);

  if (!loaded) {
    return null;
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <HomeScreen />
      </PersistGate>
    </Provider>
  );
}
